FROM docker:stable

RUN apk update && \
    apk upgrade
    
RUN apk add --no-cache vault jq libcap && setcap cap_ipc_lock= /usr/sbin/vault

RUN apk add --no-cache \
        build-base \
        libffi-dev \
        libressl-dev \
        libgcc \
        docker-compose \
        git \
        openssh-client \
        gettext
